
# Projet Infra B1 2023

Notre projet est une solution de développement décentralisée, incluant une instance gitea ainsi que plusieurs outils de monitoring et de gestion pour pouvoir mener à bien un projet de développement sans salir sa machine, ou ses profils publics de plus grands services incluant git. (Gitlab, Github) <br>

![](./ExfVrboVgAgnupC.jpg)
<br>


## Prérequis

Pour reproduire l'environnement vous aurez besoin de : <br>
- 2 machines sous Debian 11 (bullseye)
* Un nom de domaine
+ La liste des services à installer <br>


La **première machine** servira de machine principale à l'infrastructure et sera en charge d'héberger le code ainsi que les différents services.<br>
La **deuxième machine**, dans notre cas un raspberry pi sans modules hardware supplémentaires servira de serveur de sauvegarde des données importantes. Pour cela, un partage NFS sera établi entre les deux machines afin de stocker ces données ailleurs que sur le disque de la **première machine**.

## Liste des services

*La liste des services est rangée par machines, elle contient également la liste des applications à installer pour que l'environnement complet fonctionne correctement.*  <br>
<br>

### Première machine

+ [NGINX](https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/)
- [Docker & Docker compose](https://docs.docker.com/engine/install/debian/) 
+ [NodeJS & npm](https://www.rosehosting.com/blog/how-to-install-node-js-and-npm-on-debian-11/)
* [Firewalld](https://www.linuxtricks.fr/wiki/firewalld-le-pare-feu-facile-sous-linux)
- [Fail2ban](https://www.vultr.com/docs/how-to-set-up-fail2ban-on-debian-11/)
+ [Gitea](https://docs.gitea.io/en-us/installation/install-with-docker/)
- [Portainer](https://docs.portainer.io/start/install-ce/server/docker/linux)
+ [Send](https://github.com/timvisee/send)
- [uptime-kuma](https://github.com/louislam/uptime-kuma)
+ [Dashdot](https://github.com/MauriceNino/dashdot)
- [Heimdall](https://github.com/linuxserver/Heimdall)
+ [NFS CLIENT](https://www.linuxtricks.fr/wiki/debian-installer-un-serveur-nfs)
### Deuxième machine

- [UFW](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-debian-11-243261243130246d443771547031794d72784e6b36656d4a326e49732e)
+ [NFS SERVER](https://www.linuxtricks.fr/wiki/debian-installer-un-serveur-nfs)

<br>

## Installation de la base
<br>

### Docker & Docker compose
<br>

Sur la **première machine** on effectue les installations des premiers services non dockerisés avec les commandes 

```bash
sudo apt update -y && sudo apt upgrade -y
sudo apt install nginx
sudo apt remove ufw && sudo apt purge ufw
sudo apt install firewalld
sudo apt install fail2ban
```

une fois ces premières commandes entrées dans notre shell, nous pouvons, toujours sur la **première machine** débuter notre installation de docker.

cette première suite de commandes va ajouter les repositories de docker à notre machine pour pouvoir l'installer simplement depuis notre gestionnaire de paquets (apt)
```bash
 sudo apt-get install ca-certificates curl gnupg

 sudo install -m 0755 -d /etc/apt/keyrings
 curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
 sudo chmod a+r /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
Maintenant que les repos dockers sont accessibles depuis notre **première machine**, on peut simplement installer tout les composants de docker avec la commande
```sh
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
<br>

### NodeJS & npm
<br>

Pour installer nodeJs depuis la source vérifiez que vous possèfez bien curl, sinon installez le avec <br>
```sudo apt install curl```.
Ajoutez la clé pgp (remplacer 16.x par la version voulue)<br>
```curl -sL https://deb.nodesource.com/setup_16.x | bash -```

puis installez NodeJS avec la commande
```apt install nodejs```

## Configuration de base

Maintenant qu'on a installé les outils de base et mis à jour notre **première machine** nous allons faire un peu de configuration sur celle-ci, afin que tout les accès fonctionnent correctement lorsqu'un nouveau container sera déployé.<br>
<br>

### CONFIGURATION SSH
Avant toute chose, on va choisir un port (5367 ici) et l'ouvrir dans firewalld sur une connection TCP avec la commande 
```sudo firewall-cmd --add-port=5367/tcp --zone=public --permanent```

On va donc créer un nouvel utilisateur pour chaque membre du projet, qui aura son homedir et un mot de passe robuste, avant de joindre au dossier .ssh de chaque homedir la clé publique ssh (ED25519) associée à chaque ordinateur des membres du groupe de travail.
Une fois que c'est bon, on se rend dans */etc/ssh/sshd-config* pour pouvoir modifier la configuration SSH et désactiver la connexion en tant que root ainsi que l'authentification par mot de passe.

```bash
Include /etc/ssh/sshd_config.d/*.conf

Port 5367
# Authentication:

PermitRootLogin no
MaxAuthTries 5
MaxSessions 5

PubkeyAuthentication yes

AuthorizedKeysFile      .ssh/authorized_keys .ssh/authorized_keys2

PasswordAuthentication no
PermitEmptyPasswords no

UsePAM yes
X11Forwarding no
PrintMotd no
AcceptEnv LANG LC_*

# override default of no subsystems
Subsystem       sftp    /usr/lib/openssh/sftp-server
```
maintenant on redémarre notre service ssh et on peut se connecter au port défini dans la config.

<br>

### NOM DE DOMAINE 

Pour accéder à nos services depuis internet, il faut relier l'adresse ip de la machine a un nom de domaine. On va donc en louer un (ici sur porkbun.com). <br>
Notre solution étant destinée a un usage personnel et étant entièrement hebergée à domicile, de la configuration dans le routeur est également nécéssaire pour activer le port forwarding et rediriger la requête vers la **première machine**. <br>
Une fois le forwarding mis en place,un enregistrement de type A avec comme réponse au nom de domaine l'ip publique de notre routeur est nécéssaire. De cette manière, lorsqu'on tape notre nom de domaine dans un navigateur, on est renvoyé sur la **première machine**.

<br>


## DEPLOYER LES SERVICES
<br>

### EN COMMANDE
Pour une question de confort, le premier service que nous allons installer est Portainer, il ne requiert pas de configuration particulière pour l'usage que nous avons de ce service et se démarre dans un conteneur Docker assez simplement. <br>

Sa commande de lancement avec quelques options étant assez courte pour être tapée rapidement, un fichier *docker-compose* n'est pas nécéssaire même si recommandé pour ne pas avoir à retenir sa configuration. Ici nous lancerons le container directement depuis le CLI avec la commande : <br> <br>
 ```docker run -d -p 8000:8000 -p 9000:9000 --restart unless-stopped --name="portainer" -v /var/run/docker.sock:/var/run/docker.sock -v /home/dhoney/docker/portainer:/data portainer/portainer-ce```

Une fois que le container est lancé, il faut ouvrir les ports spécifiés dans la configuration du container pour pouvoir y avoir accès: <br> <br>
```sudo firewall-cmd --add-port=8000/tcp --permanent``` <br>
```sudo firewall-cmd --add-port=9000/tcp --permanent``` <br>
```sudo firewall-cmd --reload```

Maintenant que le container est accessible, on peut taper **ip_publique:port_exposé** dans notre navigateur pour accéder au conteneur, la création du compte administrateur vous sera demandée et une fois effectuée vous accederez au service. <br>

Nous pouvons donc maintenant aller dans la configuration NGINX pour y ajouter un bloc serveur redirigeant vers notre conteneur Portainer.

Dans le dossier */etc/nginx/sites-avaliable/* on crée un fichier de configuration *portainer.nomdedomaine* pour rediriger les gens qui tapent l'url **portainer.nomdedomaine**

```bash
server {
    server_name  portainer.manytoys.xyz;

    location / {
        proxy_pass http://localhost:9000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
    }
}
```

Pour activer la norme de connexion chiffrée TLS, nous avons beson d'installer le certbot de letsencrypt afin d'obtenir des certificats SSL gratuits. 

```sudo apt install certbot python3-certbot-nginx``` <br>
```sudo certbot --nginx -d portainer.manytoys.xyz``` <br>

Le certificat s'installe automatiquement dans le bon fichier de configuration et une fois la commande executée notre bloc serveur ressemble à ça: 

```bash
server {
    server_name  portainer.manytoys.xyz;

    location / {
        proxy_pass http://localhost:9000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
    }

    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/portainer.manytoys.xyz/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/portainer.manytoys.xyz/privkey.pem; # managed by Certbot    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = portainer.manytoys.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;
    server_name  portainer.manytoys.xyz;
    return 404; # managed by Certbot


}
```
Nous pouvons copier le fichier de configuration dans le dossier */etc/nginx/sites-enabled* avant de redémarrer NGINX pour activer la redirection. Maintenant, il faut configurer un nouvel enregistrement **portainer.manytoys.xyz** sur notre nom de domaine qui lors d'une requête vers ce dernier renverra l'utilisateur sur la même ip publique. Il sera maintenant dirigé vers Portainer, aura sa connexion chiffrée avec TLS et pourra accéder au service avec une url. <br> <br>

### AVEC UN DOCKER COMPOSE

Le projet regroupant un nombre conséquent de services et la plupart d'entre eux ne nécéssitant pas particulièrement de configuration. Seul certains containers (notamment gitea pour sa configuration) ont eu besoin d'être lancés depuis un fichier *docker-compose.yml*

```yml
version: "3"

networks:
  gitea:
    external: false

services:
  server:
    image: gitea/gitea:latest
    container_name: gitea
    environment:
      - USER_UID=106
      - USER_GID=113
    restart: always
    networks:
      - gitea
    volumes:
      - ./:/data
      - /home/git/.ssh/:/data/git/.ssh
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "127.0.0.1:3000:3000"
      - "127.0.0.1:2222:22"
```
Une fois que le container est lancé, il faut ouvrir les ports spécifiés dans la configuration du container pour pouvoir y avoir accès:  <br>
```sudo firewall-cmd --add-port=3000/tcp --permanent``` <br>
```sudo firewall-cmd --reload```

Pour démarrer le conteneur Gitea sur la machine et obtenir la même configuration que sur l'instance disponible à [notre Gitea](https://dev.manytoys.xyz) nous avons placés le fichier *docker-compose.yml* dans un dossier Gitea, créé dans le repertoire destiné à tout nos autres services puis tapé la commande ```docker-compose up -d```. Le container s'est correctement build, nous avons accédé la première fois à notre gitea de la même façon que dans la méthode *PAR COMMANDE* pour effectuer la configuration de base avant de refaire un bloc serveur similaire, dans un autre fichier portant le même nom que celui du sous-domaine attribué a notre Gitea. 

```bash
server {
    server_name dev.manytoys.xyz;

    location / {
        client_max_body_size 80M;
        proxy_pass http://localhost:3000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
    }

}
```
nous effectuons a nouveau la commande certbot pour créer un certificat pour notre service. <br>
```sudo certbot --nginx -d portainer.manytoys.xyz``` <br>

notre bloc serveur à de nouveau été configuré correctement et nous pouvons copier le fichier de configuration dans le dossier */etc/nginx/sites-enabled* avant de redémarrer NGINX pour activer la redirection.

```bash
server {
    server_name dev.manytoys.xyz;

    location / {
        client_max_body_size 80M;
        proxy_pass http://localhost:3000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
    }


    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/dev.manytoys.xyz/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/dev.manytoys.xyz/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot


}
server {
    if ($host = dev.manytoys.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    server_name dev.manytoys.xyz;
    listen 80;
    return 404; # managed by Certbot
}
```
Il faut ensuite aller créer un enregistrement pour le sous-domaine chez notre fournisseur de nom de domaine.<br>

**Ces manipulations sont à effectuer pour chacun des services déployés** <br> <br>

Nous avons ensuite été dans le dossier monté sur notre disque pour modifier les fichiers de base de gitea, nous avons notamment modifié la page d'accueil disponible au repertoire *docker/gitea/gitea/templates* dans le fichier **home.tmpl**

```html
{{template "base/head" .}}
                            ...
                                        {{AppName}}
                                </h1>
                                <h2>Rule the world</h2>
                        </div>
                </div>
        </div>
...
{{template "base/footer" .}}
```

ainsi que les images de bases et themes du site disponibles aux repertoires *docker/gitea/gitea/public/img* et *docker/gitea/gitea/public/css*

<br>

### AUTRES SERVICES

Comme il y a un nombre conséquent de conteneurs à déployer et que les méthodes restent les mêmes à l'exception des configs qui sont différentes pour chacun des services, 
nous allons énumérer la liste des services avec leur configuration ci-dessous, pour un gain de temps dans la lecture. <br>
<br>

#### HEIMDALL

*commande cli* : ```docker run -d --name=heimdall -v /home/dhoney/docker/heimdall:/config -e PGID=1000 -e PUID=1000 -p 8080:80 -p 8443:443 --restart unless-stopped linuxserver/heimdall```

*bloc serveur* : 
```bash
server {
    server_name  manytoys.xyz;

    location / {
        proxy_pass https://localhost:8443;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
    }


    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/manytoys.xyz/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/manytoys.xyz/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}

server {
    if ($host = manytoys.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;
    server_name  manytoys.xyz;
    return 404; # managed by Certbot


}
```
<br>
<br>

#### UPTIME-KUMA

*commande cli*: ```docker run -d --restart=always -p 3001:3001 -v $PWD/uptime-kuma:/app/data --name uptime-kuma louislam/uptime-kuma:1```

*bloc serveur*: 
```bash
server {
    server_name status.manytoys.xyz;

    location / {
        proxy_pass http://localhost:3001;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }

    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/status.manytoys.xyz/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/status.manytoys.xyz/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = status.manytoys.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;
    server_name status.manytoys.xyz;
    return 404; # managed by Certbot


}
```
<br>
<br>

#### Dashdot

*commande cli*: ```docker container run -d -p 3002:3002 --privileged -v /:/mnt/host:ro --env DASHDOT_ENABLE_CPU_TEMPS="true" --env DASHDOT_PORT="3002" mauricenino/dashdot```

*bloc serveur*:
```bash
server {
    server_name monit.manytoys.xyz;

    location / {
        proxy_pass http://localhost:3002;
        proxy_hide_header Upgrade;
    }


    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/monit.manytoys.xyz/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/monit.manytoys.xyz/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = monit.manytoys.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;
    server_name monit.manytoys.xyz;
    return 404; # managed by Certbot


}
```

#### Mozilla Send

*commande cli*: ```docker run --name=mozillasend -v $PWD/send:/uploads -p 1443:1443  -e 'DETECT_BASE_URL=true' --restart unless-stopped  -e 'REDIS_HOST=localhost'  registry.gitlab.com/timvisee/send:latest```

*bloc serveur*
```bash
server {
    server_name  send.manytoys.xyz;

    location / {
        proxy_pass http://localhost:1443;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
    }

    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/send.manytoys.xyz/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/send.manytoys.xyz/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}

server {
    if ($host = send.manytoys.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;
    server_name  send.manytoys.xyz;
    return 404; # managed by Certbot


}
```

### CONFIG FAIL2BAN

Rappel de la commande d'installation
`
sudo apt install fail2ban
`

On va réaliser une configuration très basique.
Vous pouvez voir les configurations de base dans `/etc/fail2ban/jail.conf` et il est GRANDEMENT préférable de ne pas modifier ce fichier directmement car il sera overwrite par les mises à jour. On va plutôt le copier/coller ici `/etc/fail2ban/jail.local` et y rajouter cette configuration. Pour enlever toutes les lignes commentées du fichier sur vim (:g/#/d).

Configuration
```
[DEFAULT]

ignore = votreip
findtime = 20m
bantime = 3h
maxretry = 5

[sshd]

enabled = true

[nginx-http-auth]

enabled = true
action = discord_notifications

[nginx-noscript]

enabled = true
port = hhtp,https
filter = nginx-noscript
logpath = /var/log/nginx/access.log
action = discord_notifications
```

Ignoreip va tout simplement ignore le/les ip(s) écrites càd que si votre ip est dedans vous ne pourrez aps être banni par fail2ban. (pour ignorer plusieurs ip il faut simplement mettre un espace entre elles.)
Maxretry est le nombre d'essais maximum.
Findtime est le temps qui limite ce nombre d'essais, dans le cas de la config ci-dessus si vous vous trompez 5 fois en l'espace de 20m vous serez bannis.
Et bantime est le temps de banissement dans ce cas 3h.

Enabled permet simplement d'activer la surveillance de fail2ban sur la jail.

L'action = discord_notifications, va nous permettre d'avoir les notifications dans des Webhooks ce qui est optionnel. [Webhooks](#webhooks)

Il faut maintenant créer e filter nginx-noscript dans `/etc/fail2ban/filter.d/nginx-noscript.conf`

```
[Definition]

failregex = ^<HOST> -.*GET.*(\.php|\.asp|\.exe|\.pl|\.cgi|\scgi)

ignoreregex =
```
Ce script bloque les ips voulant executant des scrips contenant les extensions .php,.asp,.exe...

Maintenant c'est le moment de démarrer le service `sudo systemctl start fail2ban`

Vous pouvez voir le nombre de jails avec `sudo fail2ban-server status` et des informations plus précises avec `sudo fail2ban-server status sshd` pour sshd.

### Webhooks

Pour les webhooks discord, il faudra créer le fichier suivant `/etc/fail2ban/action.d/discord_notifications.conf` avec cette configuration.

```
[Definition]

# Notify on Startup
actionstart = curl -X POST "<webhook>" \
            -H "Content-Type: application/json" \
            -d '{"username": "Fail2Ban", "content":":white_check_mark: The **[<name>]** jail has started"}'

# Notify on Shutdown
actionstop = curl -X POST "<webhook>" \
            -H "Content-Type: application/json" \
            -d '{"username": "Fail2Ban", "content":":no_entry: The **[<name>]** jail has been stopped"}'

#
actioncheck =

# Notify on Banned 
actionban = curl -X POST "<webhook>" \
            -H "Content-Type: application/json" \
            -d '{"username":"Fail2Ban", "content":":bell: Hey <discord_userid>! **[<name>]** :hammer:**BANNED**:hammer: IP: `<ip>` for <bantime> hours after **<failures>** failure(s). Here is some info about the IP: https://db-ip.com/<ip>"}' 
            curl -X POST "<webhook>" \
            -H "Content-Type: application/json" \
            -d '{"username":"Fail2Ban", "content":"If you want to unban the IP run: `fail2ban-client unban <ip>`"}'

# Notify on Unbanned
actionunban = curl -X POST "<webhook>" \
            -H "Content-Type: application/json" \
            -d '{"username":"Fail2Ban", "content":":bell: **[<name>]** **UNBANNED** IP: [<ip>](https://db-ip.com/<ip>)"}'
[Init]

# Name of the jail in your jail.local file. default = [your-jail-name]
name = default

# Discord Webhook URL
webhook = tonWebhookUrl

```

Sur discord il faudra aller dans Server-settings/Integrations et créer un nouveau webhook puis copier l'url de ce webhook et remplacer la dernier ligne du fichier `tonWebhookUrl` par l'url.

### BACKUP ET RESTORE

Attention, dans la version actuelle de ces scripts il est très préférable de ne pas s y fier, ils devraient cependant bientôt être modifiés.

`/user/backup.sh`

```
#!/bin/bash

i=1

for j in 1 2 3 
do
	mkdir ../backups/backup$j 2> /dev/null
	mkdir ../backups/backup$j/gitea 2> /dev/null
	mkdir ../backups/backup$j/gitea/repositories 2> /dev/null
	mkdir ../backups/backup$j/gitea/conf 2> /dev/null
	mkdir ../backups/backup$j/gitea/public 2> /dev/null
	mkdir ../backups/backup$j/gitea/templates 2> /dev/null
	mkdir ../backups/backup$j/gitea/docker-compose 2> /dev/null
	mkdir ../backups/backup$j/uptime-kuma 2> /dev/null
	mkdir ../backups/backup$j/gitea/db 2> /dev/null
done

while :
do

	echo "starting the backup for the day"
	if [[ "$i" == 4 ]] 
	then
		$i = 1
	fi

	cp -f ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/gitea/gitea.db ../backups/backup$i/gitea/db/
	cp -rT ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/uptime-kuma/ ../backups/backup$i/uptime-kuma/
	cp -f ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/docker-compose.yml ../backups/backup$i/gitea/docker-compose/
	cp -rT ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/gitea/templates/ ../backups/backup$i/gitea/templates/
	cp -rT ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/gitea/public/ ../backups/backup$i/gitea/public/
	cp -rT ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/git/repositories/ ../backups/backup$i/gitea/repositories/
	cp -f ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/gitea/conf/app.ini ../backups/backup$i/gitea/conf/
	echo "backup of the day ended"
	((i++))
	
	sleep 1d
done
```

`/user/restore.sh`

```
#!/bin/bash

echo "which backup do you want to restore ? (1/2/3/)"

read i

mkdir ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/git/repositories 2> /dev/null
mkdir ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/uptime-kuma 2> /dev/null
mkdir ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/gitea/gitea/templates 2> /dev/null
mkdir ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/gitea/public 2> /dev/null
mkdir ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/gitea/conf 2> /dev/null

cp -f ../backups/backup$i/gitea/db/gitea.db ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/gitea/
cp -rT ../backups/backup$i/uptime-kuma/ ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/uptime-kuma/
cp -f ../backups/backup$i/gitea/docker-compose/docker-compose.yml ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/
cp -rT ../backups/backup$i/gitea/templates/ ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/gitea/templates/
cp -rT ../backups/backup$i/gitea/public/ ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/gitea/public/
cp -rT ../backups/backup$i/gitea/repositories/ ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/git/repositories/
cp -f ../backups/backup$i/gitea/conf/app.ini ../`ÀRemplacerParLeNomDeL'userDétenantLaData`/docker/gitea/gitea/conf/

echo "restore ended"
```

Le script de backup, va créer une nouvelle backup tous les 24h et en garder jusqu'à 3 simultanément. Il faut maintenant transformer ce script en service.

Premièrement il faut rendre ces scripts executables donc `sudo chmod 744 backup.ssh` et `udo chmod 744 restore.ssh`

`/etc/systemd/system/backup.service`

```
[Unit]
Description=backup Service

[Service]
ExecStart=/home/user/test.sh
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

Maintenant il faut activer le service `sudo systemctl start backup.service` et pour voir son état `sudo systemctl status backup.service`




